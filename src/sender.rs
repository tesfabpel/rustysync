use std::{
    fs::OpenOptions,
    str::FromStr,
    net::{TcpStream, SocketAddr},
    io::{Write, BufReader, Read}
};
use byteorder::{NetworkEndian, ByteOrder, ReadBytesExt};
use crate::common::{BLOCK_SIZE, ChunkStatus};
use num_traits::FromPrimitive;
use std::time::Duration;
use crc32fast::Hasher;

pub struct Sender
{
    pub fname: String,
    pub dst_host: String,
}

#[derive(Debug)]
pub struct SendError;

impl From<std::io::Error> for SendError
{
    fn from(_: std::io::Error) -> Self {
        SendError
    }
}

impl Sender
{
    pub fn run(&self) -> Result<(), SendError>
    {
        //1. open source file
        let f = OpenOptions::new()
            .read(true)
            .write(false)
            .open(self.fname.clone())?;

        let addr = SocketAddr::from_str(&self.dst_host)
            .map_err(|_| SendError)?;

        eprintln!("Attempting connection...");
        let mut sender = TcpStream::connect(addr)?;
        sender.set_read_timeout(Some(Duration::from_secs(10)))?;
        sender.set_write_timeout(Some(Duration::from_secs(10)))?;

        // 2. send header
        eprintln!("Sending header...");
        let fsize = f.metadata()?.len();
        let mut buf_fsize = [0u8; std::mem::size_of::<u64>()];
        NetworkEndian::write_u64(&mut buf_fsize, fsize);

        sender.write_all(&buf_fsize)?;

        // 3. send file
        eprintln!("Sending file...");
        let mut rdr = BufReader::new(f);

        let mut buf_body = vec![0u8; BLOCK_SIZE];
        loop
        {
            eprintln!("[Next chunk]");

            // 3a. read chunk
            let read = rdr.read(&mut buf_body)?;
            if read == 0 {
                break;
            }

            // 3b. calc & send hash
            let mut hasher = Hasher::new();
            hasher.update(&buf_body[..read]);
            let hash = hasher.finalize();

            let mut hash_buf = [0u8; std::mem::size_of::<u32>()];
            NetworkEndian::write_u32(&mut hash_buf, hash);

            eprintln!("Sending hash");
            sender.write_all(&hash_buf)?;

            // 3c. check if receiver needs to have
            // the chunk
            eprintln!("Awaiting chunk status");
            let status = sender.read_u8()?;
            let status = FromPrimitive::from_u8(status)
                .ok_or(SendError)?;

            if let ChunkStatus::Matching = status
            {
                continue;
            }

            // 3d. send chunk
            eprintln!("Sending chunk");
            sender.write_all(&buf_body[..read])?;
        }

        Ok(())
    }
}
