mod sender;
mod receiver;
mod common;

use clap::{App, AppSettings};
use clap::Arg;
use sender::Sender;
use receiver::Receiver;
use crate::common::RustySyncError;

static ARG_FILE: &str = "FILE";
static ARG_DST_HOST: &str = "DST_HOST";
static ARG_DST_FILE: &str = "DST_FILE";

fn main() -> Result<(), RustySyncError>
{
    let matches = App::new("rustysync")
        .version("0.1.0")
        .author("Fabrizio Pelosi <tesfabpel@gmail.com>")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(App::new("send")
            .arg(Arg::new(ARG_DST_HOST).required(true))
            .arg(Arg::new(ARG_FILE).required(true)))
        .subcommand(App::new("recv")
            .arg(Arg::new(ARG_DST_FILE).required(true)))
        .get_matches();

    if let Some(m) = matches.subcommand_matches("send") {
        let dst = m.value_of(ARG_DST_HOST).unwrap();
        let fname = m.value_of(ARG_FILE).unwrap();

        let s = Sender
        {
            fname: fname.to_owned(),
            dst_host: dst.to_owned(),
        };

        s.run().map_err(|_| RustySyncError)?;
    }
    else if let Some(m) = matches.subcommand_matches("recv") {
        let dst_fname = m.value_of(ARG_DST_FILE).unwrap();

        let s = Receiver
        {
            dst_fname: dst_fname.to_owned()
        };

        s.run().map_err(|_| RustySyncError)?;
    }
    else
    {
        return Err(RustySyncError);
    }

    Ok(())
}


