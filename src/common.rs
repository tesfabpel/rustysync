use num_derive::FromPrimitive;

pub const BLOCK_SIZE: usize = 16usize * 1_048_576usize; // 16MiB

#[derive(Debug)]
pub struct RustySyncError;

#[repr(u8)]
#[derive(FromPrimitive)]
pub enum ChunkStatus
{
    NotMatching = 0,
    Matching = 1
}
