use std::{
    io::Read,
    net::{TcpListener, TcpStream},
    io::Write,
    fs::OpenOptions
};
use crate::common::{BLOCK_SIZE, ChunkStatus};
use std::io::{Cursor, Seek, SeekFrom};
use byteorder::{ReadBytesExt, NetworkEndian, WriteBytesExt};
use std::time::Duration;
use crc32fast::Hasher;

pub struct Receiver
{
    pub dst_fname: String
}

#[derive(Debug)]
pub struct ReceiveError;

impl From<std::io::Error> for ReceiveError
{
    fn from(_: std::io::Error) -> Self {
        ReceiveError
    }
}

impl Receiver
{
    pub fn run(&self)
        -> Result<(), ReceiveError>
    {
        let listener = TcpListener::bind("0.0.0.0:8010")?;

        eprintln!("Awaiting for connection...");

        let conn = listener.accept()?;
        let mut stream = conn.0;
        stream.set_read_timeout(Some(Duration::from_secs(10)))?;
        stream.set_write_timeout(Some(Duration::from_secs(10)))?;

        eprintln!("Receiving header...");
        let hdr = self.recv_header(&mut stream)?;

        eprintln!("Receiving file...");
        let res = self.recv_file(hdr, &mut stream);
        if res.is_err()
        {
            eprintln!("Error receiving file...");
            Err(ReceiveError)
        }
        else
        {
            eprintln!("Received correctly!");
            Ok(())
        }
    }

    fn recv_header(&self, stream: &mut TcpStream)
        -> Result<Header, ReceiveError>
    {
        let mut buf = [0u8; std::mem::size_of::<u64>()];
        stream.read_exact(&mut buf)?;

        let mut c = Cursor::new(buf);
        let bytes = c.read_u64::<NetworkEndian>()?;

        Ok(Header {
            bytes
        })
    }

    fn recv_file(&self,
                 hdr: Header,
                 stream: &mut TcpStream)
        -> Result<(), ReceiveError>
    {
        // 1. open destination file
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(self.dst_fname.clone())?;

        f.seek(SeekFrom::Start(0))?;

        // 2. start receiving loop
        let mut buf_body = vec![0u8; BLOCK_SIZE];

        let mut skipped = 0usize;
        let mut written = 0usize;

        let mut tot_read = 0u64;
        while tot_read < hdr.bytes
        {
            // 2a. first line is hash
            eprintln!("Receive hash");
            let mut hash_buf = [0u8; std::mem::size_of::<u32>()];
            stream.read_exact(&mut hash_buf)?;
            let mut c = Cursor::new(hash_buf);
            let recv_hash = c.read_u32::<NetworkEndian>()?;

            // 2b. check if we need to receive chunk
            eprintln!("Comparing hash");
            let read = f.read(&mut buf_body)?;
            if read != 0
            {
                let mut hasher = Hasher::new();
                hasher.update(&buf_body);
                let hash = hasher.finalize();
                if hash == recv_hash
                {
                    // chunk matches, inform sender to not
                    // transfer it
                    eprintln!("Skipping chunk...");

                    stream.write_u8(ChunkStatus::Matching as u8)?;
                    tot_read += read as u64;
                    skipped += 1;
                    continue;
                }
            }

            // chunk not matching, inform sender to
            // transfer it and reset file position
            eprintln!("Receiving chunk...");

            stream.write_u8(ChunkStatus::NotMatching as u8)?;
            f.seek(SeekFrom::Current(-(read as i64)))?;

            // 2c. read body
            let read = stream.read(&mut buf_body)?;
            tot_read += read as u64;

            // 2d. write buf_body to file
            f.write_all(&buf_body[..read])?;

            written += 1;
        }

        f.flush()?;

        eprintln!();
        eprintln!("Stats:");
        eprintln!("written {0}", written);
        eprintln!("skipped {0}", skipped);

        Ok(())
    }
}

struct Header
{
    pub bytes: u64
}
