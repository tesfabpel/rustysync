# RustySync

Hacky utility to send only different chunks of a file over the network.

To receive a file:  
`./rustysync recv destination_file.bin`  
This will open a TCP server on port 8010 (for now) and awaits a connection from the sender.  

To send a file:  
`./rustysync send host:port src_file.bin`  
This will attempt to connect to the receiving end at `host:port` and will send only modified chunks at `src_file.bin`  

### TODO:
- Allow to specify custom port.
- Allow to specify a different chunk size (now it's 16MiB, but the file isn't required to be a multiple of chunk size).

### REMARKS:
Please note that this is an hacky utility (and my first project in Rust), if you're really interested in this, you're free (and welcome) to contribute! :D

